# Install

There are several ways to install:

## Autotools

If you downloaded an official release of this software, then on most POSIX systems, you can do this:

```
$ ./configure
$ make
$ sudo make install
```

If you have obtained the source code for this software from its Git repository, then you have to generate the Autotools files first:

```
$ autoreconf --install
$ automake
$ ./configure
$ make
$ sudo make install
```

### Packagers

If you are packaging this software for distribution, you probably want to do this:

```
$ make dist
```

That generates a tar.gz of only the necessary files for building and installing.
You can use that tarball to create a package for your distribution or port tree.

## Manual install

If that fails, you can manually install.
You must define the VERSION variable in the script (GNU Autotools usually does that).
Then just copy the ``help`` script into your PATH, such as ``~/bin`` or ``/usr/local/bin``, or whatever you have available to you, and then make the info file:

$ makeinfo shellp.texi -o shellp.info

Place the shellp.info file in your system's ``info`` directory (usually in ``/usr/info`` or ``/usr/share/info``).

